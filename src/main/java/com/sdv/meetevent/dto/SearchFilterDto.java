package com.sdv.meetevent.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchFilterDto {
    private String name;
    private String category;
    private String city;
    private String country;
    private Integer postalCode;
}
