package com.sdv.meetevent.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressDto {
//    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String street;
    private String city;
    private Integer postalCode;
    private String country;
    private String state;
    private String number;
    private String complement;
}

