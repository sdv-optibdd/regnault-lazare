package com.sdv.meetevent.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PartyDto {
//    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String name;
    private String description;
    private Date startTime;
    private Date endTime;
    private AddressDto address;
    private Integer maxParticipants;
    private Integer currentParticipants;
    private UserDto organizer;
    private String category;
    private Integer price;
    private List<UserDto> participants;
}
