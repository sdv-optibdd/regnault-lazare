package com.sdv.meetevent.dto;

import com.sdv.meetevent.enums.Role;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    @Schema(accessMode = Schema.AccessMode.READ_ONLY)
    private Long id;
    private String email;
    private Integer postalCode;
    private String city;
    private Integer age;
    private List<PartyDto> parties;
    private Role role;
}
