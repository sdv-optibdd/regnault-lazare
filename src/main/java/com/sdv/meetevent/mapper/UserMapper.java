package com.sdv.meetevent.mapper;

import com.sdv.meetevent.dto.PostUserDto;
import com.sdv.meetevent.dto.UserDto;
import com.sdv.meetevent.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = PartyMapper.class)
public interface UserMapper {
    // Single Object
    User toEntity(UserDto userDto);
    User toPostedEntity(PostUserDto postUserDto);
    UserDto toDto(User user);
    // List of Objects
    List<User> toEntities(List<UserDto> usersDto);

    List<UserDto> toDtos(List<User> users);
}
