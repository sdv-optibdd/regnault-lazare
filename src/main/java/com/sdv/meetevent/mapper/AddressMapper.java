package com.sdv.meetevent.mapper;

import com.sdv.meetevent.dto.AddressDto;
import com.sdv.meetevent.entity.Address;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface AddressMapper {
    Address toEntity(AddressDto addressDto);
    AddressDto toDto(Address address);
    List<Address> toEntities(List<AddressDto> addressDtos);
    List<AddressDto> toDtos(List<Address> addresses);
}
