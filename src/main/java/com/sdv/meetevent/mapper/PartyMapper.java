package com.sdv.meetevent.mapper;

import com.sdv.meetevent.dto.PartyDto;
import com.sdv.meetevent.entity.Party;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface PartyMapper {
    // Single Object
    Party toEntity(PartyDto partyDto);
    PartyDto toDto(Party party);
    // List of Objects
    List<Party> toEntities(List<PartyDto> partiesDto);
    List<PartyDto> toDtos(List<Party> parties);
}
