package com.sdv.meetevent.service;

import com.sdv.meetevent.dto.PartyDto;
import com.sdv.meetevent.dto.SearchFilterDto;
import com.sdv.meetevent.dto.UserDto;
import com.sdv.meetevent.entity.User;
import com.sdv.meetevent.exception.InvalidDataException;
import com.sdv.meetevent.mapper.PartyMapper;
import com.sdv.meetevent.mapper.UserMapper;
import com.sdv.meetevent.repository.PartyRepository;
import com.sdv.meetevent.repository.UserRepository;
import com.sdv.meetevent.specification.PartySpecification;
import lombok.AllArgsConstructor;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class PartyService {
    private final PartyRepository partyRepository;
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PartyMapper partyMapper;

    @Cacheable(value = "parties", key = "#pageable.pageNumber + '-' + #pageable.pageSize + '-' + #searchFilterDto?.name + '-' + #searchFilterDto?.category + '-' + #searchFilterDto?.city + '-' + #searchFilterDto?.country + '-' + #searchFilterDto?.postalCode")
    public List<PartyDto> getAllParties(Pageable pageable, SearchFilterDto searchFilterDto) {
        return partyMapper.toDtos(partyRepository.findAll(PartySpecification.getParties(searchFilterDto), pageable).getContent());
    }

    @CacheEvict(value = "parties", allEntries = true)
    public PartyDto createParty(PartyDto partyDto) {

        UserDto organizer = partyDto.getOrganizer();
        if (organizer == null) {
            throw new InvalidDataException("Organizer is required");
        }

        if (partyDto.getStartTime().after(partyDto.getEndTime())) {
            throw new InvalidDataException("Start time must be before end time");
        }

        return partyMapper.toDto(partyRepository.save(partyMapper.toEntity(partyDto)));
    }

    public PartyDto getPartyById(Long id) {
        return partyMapper.toDto(partyRepository.findById(id).orElse(null));
    }

    @CacheEvict(value = "parties", allEntries = true)
    public PartyDto updateParty(Long id, PartyDto partyDto) {
        // fetch the party from the database
        PartyDto party = getPartyById(id);

        // update the party with the new data
        party.setName(partyDto.getName());
        party.setDescription(partyDto.getDescription());
        party.setAddress(partyDto.getAddress());
        party.setStartTime(partyDto.getStartTime());
        party.setEndTime(partyDto.getEndTime());
        party.setMaxParticipants(partyDto.getMaxParticipants());
        party.setCurrentParticipants(partyDto.getCurrentParticipants());
        party.setCategory(partyDto.getCategory());
        party.setPrice(partyDto.getPrice());
        party.setOrganizer(partyDto.getOrganizer());
        party.setParticipants(partyDto.getParticipants());

        // save the updated party
        return partyMapper.toDto(partyRepository.save(partyMapper.toEntity(party)));
    }

    @CacheEvict(value = "parties", allEntries = true)
    public void deleteParty(Long id) {
        partyRepository.deleteById(id);
    }

    public PartyDto participateToParty(Long id, Long userId) {
        PartyDto party = getPartyById(id);
        List<UserDto> participants = party.getParticipants();
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidDataException("User not found");
        }
        UserDto userDto = userMapper.toDto(user);
        // check if the user is already participating
        if (participants.contains(userDto)) {
            throw new InvalidDataException("User is already participating");
        }
        participants.add(userDto);
        party.setParticipants(participants);
        return updateParty(id, party);
    }

    public PartyDto unparticipateToParty(Long id, Long userId) {
        PartyDto party = getPartyById(id);
        List<UserDto> participants = party.getParticipants();
        User user = userRepository.findById(userId).orElse(null);
        if (user == null) {
            throw new InvalidDataException("User not found");
        }
        UserDto userDto = userMapper.toDto(user);
        // check if the user is already participating
        if (!participants.contains(userDto)) {
            throw new InvalidDataException("User is not participating");
        }
        participants.remove(userDto);
        party.setParticipants(participants);
        return updateParty(id, party);
    }
}
