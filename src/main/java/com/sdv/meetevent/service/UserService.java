package com.sdv.meetevent.service;

import com.sdv.meetevent.dto.PostUserDto;
import com.sdv.meetevent.dto.UserDto;
import com.sdv.meetevent.mapper.UserMapper;
import com.sdv.meetevent.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    public List<UserDto> getAllUsers() {
        return userMapper.toDtos(userRepository.findAll());
    }

    public UserDto createUser(PostUserDto postUserDto) {
        postUserDto.setPassword(passwordEncoder.encode(postUserDto.getPassword()));
        return userMapper.toDto(userRepository.save(userMapper.toPostedEntity(postUserDto)));
    }
}
