package com.sdv.meetevent.service;

import com.sdv.meetevent.dto.AddressDto;
import com.sdv.meetevent.mapper.AddressMapper;
import com.sdv.meetevent.repository.AddressRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;
    private final AddressMapper addressMapper;

    public List<AddressDto> getAllAddresses() {
        return addressMapper.toDtos(addressRepository.findAll());
    }

    public AddressDto createAddress(AddressDto addressDto) {
        return addressMapper.toDto(addressRepository.save(addressMapper.toEntity(addressDto)));
    }

    public AddressDto getAddressById(Long id) {
        return addressMapper.toDto(addressRepository.findById(id).orElse(null));
    }

    public AddressDto updateAddress(Long id, AddressDto addressDto) {
        addressDto.setId(id);
        return addressMapper.toDto(addressRepository.save(addressMapper.toEntity(addressDto)));
    }

    public void deleteAddress(Long id) {
        addressRepository.deleteById(id);
    }


}
