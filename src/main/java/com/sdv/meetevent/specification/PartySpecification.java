package com.sdv.meetevent.specification;

import com.sdv.meetevent.dto.SearchFilterDto;
import com.sdv.meetevent.entity.Address;
import com.sdv.meetevent.entity.Party;
import jakarta.persistence.criteria.Join;
import jakarta.persistence.criteria.Predicate;
import org.springframework.data.jpa.domain.Specification;

import java.util.ArrayList;
import java.util.List;

public class PartySpecification {
    public static Specification<Party> getParties(SearchFilterDto filter) {
        return (root, query, criteriaBuilder) -> {
            List<Predicate> predicates = new ArrayList<>();

            if (filter.getName() != null && !filter.getName().isEmpty()) {
                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")), "%" + filter.getName().toLowerCase() + "%"));
            }

            if (filter.getCategory() != null && !filter.getCategory().isEmpty()) {
                predicates.add(criteriaBuilder.equal(root.get("category"), filter.getCategory()));
            }

            if (filter.getCity() != null && !filter.getCity().isEmpty()) {
                Join<Party, Address> addressJoin = root.join("address");
                predicates.add(criteriaBuilder.equal(addressJoin.get("city"), filter.getCity()));
            }

            if (filter.getCountry() != null && !filter.getCountry().isEmpty()) {
                Join<Party, Address> addressJoin = root.join("address");
                predicates.add(criteriaBuilder.equal(addressJoin.get("country"), filter.getCountry()));
            }

            if (filter.getPostalCode() != null) {
                Join<Party, Address> addressJoin = root.join("address");
                predicates.add(criteriaBuilder.equal(addressJoin.get("postalCode"), filter.getPostalCode()));
            }

            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        };
    }
}
