package com.sdv.meetevent.exception;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ErrorDetails {
    private String message;

    public ErrorDetails(String message) {
        this.message = message;
    }
}

