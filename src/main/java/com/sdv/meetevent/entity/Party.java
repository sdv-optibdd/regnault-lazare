package com.sdv.meetevent.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.BatchSize;

import java.util.Date;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "party", indexes = {
        @Index(name = "idx_party_name", columnList = "name"),
        @Index(name = "idx_party_start_time", columnList = "startTime"),
        @Index(name = "idx_party_end_time", columnList = "endTime"),
        @Index(name = "idx_party_category", columnList = "category"),
})
public class Party {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(nullable = false)
    private Date startTime;

    @Column()
    private Date endTime;

    @Column()
    private Integer maxParticipants;

    @Column()
    private Integer currentParticipants;

    @Column(nullable = false)
    private String category;

    @Column(nullable = false)
    private Integer price = 0;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organizer_id")
    private User organizer;

    @ManyToMany(fetch = FetchType.LAZY)
    @BatchSize(size = 10)
    private List<User> participants;
}
