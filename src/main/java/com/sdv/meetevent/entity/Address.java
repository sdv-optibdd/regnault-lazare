package com.sdv.meetevent.entity;

import com.sdv.meetevent.enums.AddressCountry;
import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Table(indexes = {
        @Index(name = "idx_address_city", columnList = "city"),
        @Index(name = "idx_address_postal_code", columnList = "postalCode"),
        @Index(name = "idx_address_country", columnList = "country")
})
public class Address {
    @Id
    @GeneratedValue(strategy = jakarta.persistence.GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String street;

    @Column(nullable = false)
    private String city;

    @Column(nullable = false)
    private Integer postalCode;

    @Enumerated(EnumType.STRING)
    private AddressCountry country = AddressCountry.FRANCE;

    @Column()
    private String state;

    @Column()
    private String number;

    @Column()
    private String complement;
}
