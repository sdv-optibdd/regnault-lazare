package com.sdv.meetevent.repository;

import com.sdv.meetevent.entity.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
