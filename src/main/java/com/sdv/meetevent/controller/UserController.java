package com.sdv.meetevent.controller;

import com.sdv.meetevent.dto.PostUserDto;
import com.sdv.meetevent.dto.UserDto;
import com.sdv.meetevent.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping
    public UserDto createUser(@RequestBody PostUserDto postUserDto) {
        return userService.createUser(postUserDto);
    }
}
