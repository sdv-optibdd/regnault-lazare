package com.sdv.meetevent.controller;

import com.sdv.meetevent.dto.PartyDto;
import com.sdv.meetevent.dto.SearchFilterDto;
import com.sdv.meetevent.service.PartyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/party")
public class PartyController {

    @Autowired
    private PartyService partyService;

    @GetMapping()
    public List<PartyDto> getParties(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size,
            @RequestParam(required = false) String name,
            @RequestParam(required = false) String category,
            @RequestParam(required = false) String city,
            @RequestParam(required = false) String country,
            @RequestParam(required = false) Integer postalCode
    ) {
        Pageable pageable = PageRequest.of(page, size);
        SearchFilterDto searchFilterDto = new SearchFilterDto(name, category, city, country, postalCode);
        return partyService.getAllParties(pageable, searchFilterDto);
    }

    @PostMapping
    public PartyDto postParty(@RequestBody PartyDto partyDto) {
        return partyService.createParty(partyDto);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PartyDto> getParty(@PathVariable Long id) {
        return ResponseEntity.ok(partyService.getPartyById(id));
    }

    @PatchMapping("/{id}")
    public PartyDto patchParty(@PathVariable Long id, @RequestBody PartyDto partyDto) {
        return partyService.updateParty(id, partyDto);
    }

    @DeleteMapping("/{id}")
    public void deleteParty(@PathVariable Long id) {
        partyService.deleteParty(id);
    }

    @PatchMapping("/{id}/participate")
    public PartyDto participateToParty(@PathVariable Long id, @RequestBody Long userId) {
        return partyService.participateToParty(id, userId);
    }

    @PatchMapping("/{id}/unparticipate")
    public PartyDto unparticipateToParty(@PathVariable Long id, @RequestBody Long userId) {
        return partyService.unparticipateToParty(id, userId);
    }
}
