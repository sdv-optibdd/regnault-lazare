package com.sdv.meetevent.enums;

public enum Role {
    USER,
    ADMIN
}
